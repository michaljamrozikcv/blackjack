package com.example.blackJack.gamers;

import com.example.blackJack.deck.PlayingCard;

public interface Playable {
    void hit(PlayingCard card);
    void resetCards();
}
