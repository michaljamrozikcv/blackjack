package com.example.blackJack.gamers;

import com.example.blackJack.deck.PlayingCard;

import java.util.ArrayList;
import java.util.List;

public class Dealer implements Playable {
    private int cardsValue;
    private List<PlayingCard> listOfCards = new ArrayList<>();
    private int quantityOfAces;//Aces with value of 11

    public int getQuantityOfAces() {
        return quantityOfAces;
    }

    public int getCardsValue() {
        return cardsValue;
    }

    private void setCardsValue(int cardsValue) {
        this.cardsValue = cardsValue;
    }

    public List<PlayingCard> getListOfCards() {
        return listOfCards;
    }

    private void setQuantityOfAces(int quantityOfAces) {
        this.quantityOfAces = quantityOfAces;
    }

    @Override
    public void hit(PlayingCard card) {
        listOfCards.add(card);
        if (card.getValue() == 11) {
            quantityOfAces++;
        }
        cardsValue += card.getValue();
        while (quantityOfAces > 0 && cardsValue > 21) {
            cardsValue -= 10;
            quantityOfAces--;
        }
    }

    @Override
    public void resetCards() {
        listOfCards.clear();
        setCardsValue(0);
        setQuantityOfAces(0);
    }
}
