package com.example.blackJack.gamers;

import com.example.blackJack.deck.PlayingCard;

import java.util.ArrayList;
import java.util.List;

public class Player implements Playable {
    private final String nick;

    private double buyIn;
    private double stack;
    private double bet;
    private double betSplit;
    private int cardsValue;
    private int cardsValueSplit;
    private int quantityOfAces;
    private int quantityOfAcesSplit;
    private List<PlayingCard> listOfCards = new ArrayList<>();
    private List<PlayingCard> listOfCardsSplit = new ArrayList<>();

    public Player(String nick) {
        this.nick = nick;
    }

    public int getCardsValueSplit() {
        return cardsValueSplit;
    }

    public double getBetSplit() {
        return betSplit;
    }

    private void setBetSplit(double betSplit) {
        this.betSplit = betSplit;
    }

    private void setCardsValueSplit(int cardsValueSplit) {
        this.cardsValueSplit = cardsValueSplit;
    }

    private void setQuantityOfAcesSplit(int quantityOfAcesSplit) {
        this.quantityOfAcesSplit = quantityOfAcesSplit;
    }

    public List<PlayingCard> getListOfCardsSplit() {
        return listOfCardsSplit;
    }


    private void setQuantityOfAces(int quantityOfAces) {
        this.quantityOfAces = quantityOfAces;
    }

    public String getNick() {
        return nick;
    }

    public double getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(double buyIn) {
        this.buyIn = buyIn;
    }

    public double getStack() {
        return stack;
    }

    public void setStack(double stack) {
        this.stack = stack;
    }

    public double getBet() {
        return bet;
    }

    public void setBet(double bet) {
        this.bet = bet;
    }

    public int getCardsValue() {
        return cardsValue;
    }

    private void setCardsValue(int cardsValue) {
        this.cardsValue = cardsValue;
    }

    public List<PlayingCard> getListOfCards() {
        return listOfCards;
    }

    public void removeFromStack(double bet) {
        stack -= bet;
    }

    public void hitSplit(PlayingCard card) {
        listOfCardsSplit.add(card);
        if (card.getValue() == 11) {
            quantityOfAcesSplit++;
        }
        cardsValueSplit += card.getValue();
        while (quantityOfAcesSplit > 0 && cardsValueSplit > 21) {
            cardsValueSplit -= 10;
            quantityOfAcesSplit--;
        }
    }

    public void split() {
        listOfCardsSplit.add(listOfCards.get(1));
        setCardsValue(listOfCards.get(0).getValue());
        listOfCards.remove(1);
        setCardsValueSplit(listOfCardsSplit.get(0).getValue());
        setBetSplit(getBet());
        setStack(getStack() - getBetSplit());
    }

    @Override
    public String toString() {
        return "stack: $" + stack;
    }

    @Override
    public void hit(PlayingCard card) {
        listOfCards.add(card);
        if (card.getValue() == 11) {
            quantityOfAces++;
        }
        cardsValue += card.getValue();
        while (quantityOfAces > 0 && cardsValue > 21) {
            cardsValue -= 10;
            quantityOfAces--;
        }
    }

    @Override
    public void resetCards() {
        listOfCards.clear();
        listOfCardsSplit.clear();
        setCardsValue(0);
        setCardsValueSplit(0);
        setQuantityOfAces(0);
        setQuantityOfAcesSplit(0);
    }
}





