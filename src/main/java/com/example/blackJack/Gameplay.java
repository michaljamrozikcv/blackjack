package com.example.blackJack;

import com.example.blackJack.deck.Deck;
import com.example.blackJack.deck.PlayingCard;
import com.example.blackJack.gamers.Dealer;
import com.example.blackJack.gamers.Playable;
import com.example.blackJack.gamers.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.stream.Collectors;

public class Gameplay {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_YELLOW = "\u001b[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_MAGENTA = "\u001b[35m";

    private final int quantityOfDecks;
    private final Player player;
    private final Dealer dealer = new Dealer();
    private final GameplayOptions gameplayOptions = new GameplayOptions();

    private List<PlayingCard> dealingShoe;
    private int round = 1;

    private Gameplay(Player player, int quantityOfDecks) {
        this.player = player;
        this.quantityOfDecks = quantityOfDecks;
    }

    public Player getPlayer() {
        return player;
    }

    /**
     * Create one player,  add buy-in and create dealing shoe - it is simulations of dealer behavior -
     * the dealer unbox 1-8 brand new decks, quantity of decks is random - not know by player
     */
    public static Gameplay createGameplay(Scanner scanner) {
        System.out.println("\n--------------------------------------------------------------------------");
        System.out.println("*** Welcome to BLACKJACK online Casino!!!!! ***");
        System.out.println("--------------------------------------------------------------------------");
        System.out.print(ANSI_MAGENTA + "Please enter your nick: " + ANSI_RESET);
        String nick = scanner.nextLine();
        Gameplay gameplay = new Gameplay(new Player(nick), new Random().nextInt(8) + 1);
        gameplay.placeBuyIn(scanner);
        System.out.println("");
        gameplay.createDealingShoe();
        return gameplay;
    }

    /**
     * Provide buy-in amount, it examines cases when player by mistake provides text or non-positive amount.
     */
    private void placeBuyIn(Scanner scanner) {
        boolean flag = true;
        double amount = 0;
        System.out.print(ANSI_MAGENTA + "Please enter buy-in $ amount (-1 for exit): " + ANSI_RESET);
        while (flag) {
            try {
                amount = scanner.nextDouble();
                flag = false;
                if (amount <= 0 && amount != -1) {
                    System.out.print(ANSI_MAGENTA + "Value has to be positive. " +
                            "Please re-enter buy-in $ amount  (-1 for exit):  " + ANSI_RESET);
                    flag = true;
                } else if (amount == -1) {
                    printHeader("See you soon. Good bye.");
                    System.exit(1);
                }
            } catch (InputMismatchException exception) {
                System.out.print(ANSI_MAGENTA + "System cannot recognize amount. " +
                        "Please re-enter buy-in $ amount  (-1 for exit): " + ANSI_RESET);
                scanner.nextLine();
            }
        }
        player.setBuyIn(amount);
        player.setStack(amount);
    }

    /**
     * shuffle all chosen decks and put into dealing shoe
     */
    private void createDealingShoe() {
        List<Deck> listOfDecks = new ArrayList<>();
        for (int i = 0; i < quantityOfDecks; i++) {
            listOfDecks.add(new Deck());
        }
        dealingShoe = listOfDecks.stream()
                .flatMap(deck -> deck.getListOfCards().stream())
                .collect(Collectors.toList());
        Collections.shuffle(dealingShoe);
    }

    public void particularRound(Scanner scanner) {
        roundInitialization(scanner);
        //blackjack cases after dealing 2 cards to player and dealer
        if (dealer.getCardsValue() == 21 && player.getCardsValue() != 21) {
            displayAllCardsDealer();
            gameplayOptions.showWinner(dealer, "Dealer has blackjack, ", 0);
        } else if (dealer.getCardsValue() != 21 && player.getCardsValue() == 21) {
            displayAllCardsDealer();
            gameplayOptions.showWinner(player, "Blackjack!!! ", 2.5);
        } else if (dealer.getCardsValue() == 21 && player.getCardsValue() == 21) {
            displayAllCardsDealer();
            gameplayOptions.showWinner(null, " have blackjack.", 0);
        } else {
            //no blackjack cases after dealing 2 cards to player and dealer
            if (player.getStack() >= player.getBet()
                    && player.getListOfCards().get(0).getRank().equals(player.getListOfCards().get(1).getRank())) {
                gameplayOptions.playerFourOptionsCase(scanner);
            } else if (player.getStack() >= player.getBet()) {
                gameplayOptions.playerThreeOptionsCase(scanner);
            } else {
                gameplayOptions.playerTwoOptionsCase(scanner);
            }
        }
        dealer.resetCards();
        player.resetCards();
        round++;
        if (player.getStack() == 0) {
            //game over if no money in stack
            gameExit();
        }
    }

    /**
     * Initialization of particular round - player provides bet amount; then deal 2 cards to player and dealer
     * and display them
     */
    private void roundInitialization(Scanner scanner) {
        printHeader("Round: " + round);
        System.out.println(player);
        placeBet(scanner);
        dealTwoCards();
    }

    /**
     * Provide bet amount (or exit game), it examines cases when player by mistake, provides text, non-positive
     * amount or amount higher than his stack.
     */
    private void placeBet(Scanner scanner) {
        boolean flag = true;
        double amount = 0;
        System.out.print(ANSI_MAGENTA + "Place your bet (-1 for exit): " + ANSI_RESET);
        while (flag) {
            try {
                amount = scanner.nextDouble();
                flag = false;
                if (amount <= 0 && amount != -1) {
                    System.out.print(ANSI_MAGENTA + "Value has to be positive. " +
                            "Please re-enter bet $ amount  (-1 for exit):  " + ANSI_RESET);
                    flag = true;
                } else if (amount == -1) {
                    gameExit();
                } else if (amount > player.getStack()) {
                    System.out.print(ANSI_MAGENTA + "Bet cannot be higher that your stack!!!" +
                            " Please re-enter bet $ amount  (-1 for exit): " + ANSI_RESET);
                    flag = true;
                }
            } catch (InputMismatchException exception) {
                System.out.print(ANSI_MAGENTA + "System cannot recognize amount. " +
                        "Please re-enter bet $ amount  (-1 for exit): " + ANSI_RESET);
                scanner.nextLine();
            }
        }
        player.setBet(amount);
        player.removeFromStack(amount);
    }

    /**
     * Deal 4 cards at the beginning of a round - 2 to player and 2 to dealer; exit game if in dealing
     * shoe there are no enough cards
     */
    private void dealTwoCards() {
        gameOverNoCards(4, "Sorry, not enough cards in dealing shoe to play next round.");
        for (int i = 0; i < 2; i++) {
            player.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
            dealer.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
        }
    }

    /**
     * Game is automatically over if there are less cards in dealing shoe than provided parameter
     *
     * @param quantityOfCards - quantity of cards remaining in dealing shoe
     * @param text            - message printed when game is over
     */
    private void gameOverNoCards(int quantityOfCards, String text) {
        if (dealingShoe.size() < quantityOfCards) {
            player.setStack(player.getStack() + player.getBet());
            System.out.println(ANSI_RED + text + ANSI_RESET);
            gameExit();
        }
    }

    private void gameExit() {
        printHeader("GAME OVER - thank you " + player.getNick() + "!!!!! Good bye.");
        System.out.println("Your buy-in: $" + player.getBuyIn() +
                "\nYour stack: $" + player.getStack());
        if (player.getBuyIn() <= player.getStack()) {
            System.out.println("You won: " + ANSI_BLUE + "$" + (player.getStack() - player.getBuyIn()) + ANSI_RESET);
        } else {
            System.out.println("You lost: " + ANSI_RED + "$" + (Math.abs(player.getStack() - player.getBuyIn())) + ANSI_RESET);
        }
        System.exit(1);
    }

    /**
     * Display Dealer's cards, his second card is face-down
     */
    private void displayFirstCardDealer() {
        System.out.println("==================================================");
        System.out.println(("Dealer: [" + dealer.getListOfCards().get(0) + ", X]  " +
                "value: " + dealer.getListOfCards().get(0).getValue()));
    }

    private void displayAllCardsDealer() {
        System.out.println("==================================================");
        System.out.println(("Dealer: " + dealer.getListOfCards() +
                "  value: " + dealer.getCardsValue()));
    }

    private void displayAllCardsPlayer() {
        printHeaderLowerTop(player.getNick() + ": " + player.getListOfCards() + "  value: " + player.getCardsValue());
    }

    /**
     * display player's cards when option "split" has been chosen
     */
    private void displayAllCardsPlayerSplit() {
        if (player.getCardsValue() > 21 && player.getCardsValueSplit() > 21) {
            displayMessagePlayerSplit("  lost, value higher than 21", "  lost, value higher than 21");
        } else if (player.getCardsValue() > 21) {
            displayMessagePlayerSplit("  lost, value higher than 21", "");
        } else if (player.getCardsValueSplit() > 21) {
            displayMessagePlayerSplit("", "  lost, value higher than 21");
        } else {
            displayMessagePlayerSplit("", "");
        }
    }

    /**
     * display player's list of cards and messages in case of split
     *
     * @param text1 - message to be printed for split1
     * @param text2 - message to be printed for split1
     */
    private void displayMessagePlayerSplit(String text1, String text2) {
        printHeaderLowerTop(player.getNick() + "-split 1: " + player.getListOfCards() + "  value: "
                + player.getCardsValue() + "  bet: $" + player.getBet() + ANSI_RED + text1 + ANSI_RESET +
                "\n" + player.getNick() + "-split 2: " + player.getListOfCardsSplit() + "  value: "
                + player.getCardsValueSplit() + "  bet: $" + player.getBetSplit() + ANSI_RED + text2 + ANSI_RESET);
    }

    private void printHeader(String text) {
        System.out.println("==========================================================================");
        System.out.println(" *** " + text + " ***");
        System.out.println("==========================================================================");
    }

    private void printHeaderLowerTop(String text) {
        System.out.println("--------------------------------------------------");
        System.out.println(text);
    }

    private class GameplayOptions {
        /**
         * At very beginning - if no blackjacks and first two cards is a pair, player has 4 options -
         * hit, stand, double (supposing his remaining stack is equal to bet at least), split.
         */
        private void playerFourOptionsCase(Scanner scanner) {
            displayFirstCardDealer();
            displayAllCardsPlayer();
            int option = getOptionFourOptionsCase(scanner);
            switch (option) {
                case 1:
                    playerHit(scanner);
                    playerTwoOptionsCase(scanner);
                    break;
                case 2:
                    dealerOptions();
                    break;
                case 3:
                    playerDouble();
                    break;
                case 4:
                    playerSplit(scanner);
                    break;
            }
        }

        /**
         * Logic depending on which option number has been chosen (1-hit, 2-stand, 3-double, 4-split)
         */
        private int getOptionFourOptionsCase(Scanner scanner) {
            int option = 1;
            boolean flag = true;
            while (flag) {
                System.out.println(ANSI_MAGENTA + "*** Press: 1-hit, 2-stand, 3-double, 4-split ***" + ANSI_RESET);
                try {
                    option = scanner.nextInt();
                    flag = false;
                    if (option != 1 && option != 2 && option != 3 && option != 4) {
                        System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                        flag = true;
                    }
                } catch (InputMismatchException exception) {
                    System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                    scanner.nextLine();
                }
            }
            return option;
        }

        /**
         * At very beginning - if no blackjacks and no pair, player has 3 options - hit, stand, double (supposing
         * his remaining stack is equal to bet at least.
         */
        private void playerThreeOptionsCase(Scanner scanner) {
            displayFirstCardDealer();
            displayAllCardsPlayer();
            int option = getOptionThreeOptionsCase(scanner);
            switch (option) {
                case 1:
                    playerHit(scanner);
                    playerTwoOptionsCase(scanner);
                    break;
                case 2:
                    dealerOptions();
                    break;
                case 3:
                    playerDouble();
                    break;
            }
        }

        /**
         * Logic depending on which option number has been chosen (1-hit, 2-stand, 3-double)
         */
        private int getOptionThreeOptionsCase(Scanner scanner) {
            int option = 1;
            boolean flag = true;
            while (flag) {
                System.out.println(ANSI_MAGENTA + "*** Press: 1-hit, 2-stand, 3-double ***" + ANSI_RESET);
                try {
                    option = scanner.nextInt();
                    flag = false;
                    if (option != 1 && option != 2 && option != 3) {
                        System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                        flag = true;
                    }
                } catch (InputMismatchException exception) {
                    System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                    scanner.nextLine();
                }
            }
            return option;
        }

        /**
         * After first stage choice - player has 2 options - hit or stand (if his cards didn't exceed 21).
         * This case is also possible when at very beginning of a round player's bet is higher than a stack remaining.
         */
        private void playerTwoOptionsCase(Scanner scanner) {
            int option = 1;
            while (option == 1 && player.getCardsValue() < 21) {
                displayFirstCardDealer();
                displayAllCardsPlayer();
                boolean flag = true;
                while (flag) {
                    System.out.println(ANSI_MAGENTA + "*** Press: 1-hit, 2-stand ***" + ANSI_RESET);
                    try {
                        option = scanner.nextInt();
                        flag = false;
                        if (option != 1 && option != 2) {
                            System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                            flag = true;
                        }
                    } catch (InputMismatchException exception) {
                        System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                        scanner.nextLine();
                    }
                }
                switch (option) {
                    case 1:
                        playerHit(scanner);
                        break;
                    case 2:
                        dealerOptions();
                        break;
                }
            }
        }

        /**
         * Player hits - it means he gets one more card. If exceeded 21, dealer wins. Otherwise, dealer's turn.
         */
        private void playerHit(Scanner scanner) {
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
            if (player.getCardsValue() > 21) {
                displayFirstCardDealer();
                showWinner(dealer, "Card's value is higher than 21, ", 0);
            } else if (player.getCardsValue() == 21) {
                dealerOptions();
            }
        }

        /**
         * Player doubles his bet and get just one more card. If exceeded 21, dealer wins. Otherwise, dealer's turn.
         */
        private void playerDouble() {
            player.setStack(player.getStack() - player.getBet());
            player.setBet(player.getBet() * 2);
            System.out.println(ANSI_MAGENTA + "Doubled bet: $" + player.getBet() + ", you will get just one more card" + ANSI_RESET);
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
            if (player.getCardsValue() > 21) {
                displayFirstCardDealer();
                showWinner(dealer, "Card's value is higher than 21, ", 0);
            } else {
                dealerOptions();
            }
        }

        /**
         * If player's first two cards is a pair, player can split them and put out a second bet so that each card
         * will become the first card on two new hands.
         */
        private void playerSplit(Scanner scanner) {
            player.split();
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hitSplit(dealingShoe.get(0));
            dealingShoe.remove(0);
            if (player.getListOfCards().get(0).getValue() != 11 || player.getListOfCardsSplit().get(0).getValue() != 11) {
                //action if initial pair is not pair of Aces
                playerSplitOneOptions(scanner);
                playerSplitTwoOptions(scanner);
            }
            if (player.getListOfCards().get(0).getValue() == 11 || player.getListOfCardsSplit().get(0).getValue() == 11) {
                //if initial pair is a pair of Aces, player will get just one card to each split
                dealerOptionsSplit();
            }
        }

        /**
         * Logic for split1 - depending on which option number has been chosen (1-hit, 2-stand)
         */
        private void playerSplitOneOptions(Scanner scanner) {
            int option = 1;
            while (option == 1 && player.getCardsValue() < 21) {
                displayFirstCardDealer();
                displayAllCardsPlayerSplit();
                boolean flag = true;
                while (flag) {
                    System.out.println(ANSI_MAGENTA + "*** Split 1: Press: 1-hit, 2-stand ***" + ANSI_RESET);
                    try {
                        option = scanner.nextInt();
                        flag = false;
                        if (option != 1 && option != 2) {
                            System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                            flag = true;
                        }
                    } catch (InputMismatchException exception) {
                        System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                        scanner.nextLine();
                    }
                }
                switch (option) {
                    case 1:
                        playerHitSplitOne(scanner);
                        break;
                    case 2:
                        break;
                }
            }
        }

        /**
         * Logic for split2 - depending on which option number has been chosen (1-hit, 2-stand)
         */
        private void playerSplitTwoOptions(Scanner scanner) {
            if (player.getCardsValueSplit() >= 21) {
                dealerOptionsSplit();
            } else {
                int option = 1;
                while (option == 1 && player.getCardsValueSplit() < 21) {
                    displayFirstCardDealer();
                    displayAllCardsPlayerSplit();
                    boolean flag = true;
                    while (flag) {
                        System.out.println(ANSI_MAGENTA + "*** Split 2: Press: 1-hit, 2-stand ***" + ANSI_RESET);
                        try {
                            option = scanner.nextInt();
                            flag = false;
                            if (option != 1 && option != 2) {
                                System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                                flag = true;
                            }
                        } catch (InputMismatchException exception) {
                            System.out.println(ANSI_MAGENTA + "Wrong choice. Please try once again." + ANSI_RESET);
                            scanner.nextLine();
                        }
                    }
                    switch (option) {
                        case 1:
                            playerHitSplitTwo(scanner);
                            break;
                        case 2:
                            dealerOptionsSplit();
                            break;
                    }
                }
            }
        }

        /**
         * Player hit card in split 1
         */
        private void playerHitSplitOne(Scanner scanner) {
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hit(dealingShoe.get(0));
            dealingShoe.remove(0);
        }

        /**
         * Player hit card in split 2
         */
        private void playerHitSplitTwo(Scanner scanner) {
            gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
            player.hitSplit(dealingShoe.get(0));
            dealingShoe.remove(0);
            if (player.getCardsValueSplit() > 21 && player.getCardsValue() > 21) {
                //player losses if value of two splits are higher than 21
                displayFirstCardDealer();
                displayAllCardsPlayerSplit();
                printHeaderLowerTop(ANSI_RED + "21 exceeded in both splits, " + player.getNick() + " lost" + ANSI_RESET);
            } else if (player.getCardsValueSplit() >= 21) {
                dealerOptionsSplit();
            }
        }

        /**
         * The dealer hit if lower than 17 or soft 17 (incl. Ace with value of 11). End of game if no cards
         * in dealing shoe.
         */
        private void dealerOptions() {
            while (dealer.getCardsValue() < 17 || (dealer.getCardsValue() == 17 && dealer.getQuantityOfAces() > 0)) {
                gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
                dealer.hit(dealingShoe.get(0));
                dealingShoe.remove(0);
                if (dealer.getCardsValue() > 21) {
                    //dealer losses if value higher than 21
                    displayAllCardsDealer();
                    showWinner(player, "Dealer's cards value is higher than 21, ", 2);
                    break;
                }
            }
            generalWinCases();
        }

        private void dealerOptionsSplit() {
            while (dealer.getCardsValue() < 17 || (dealer.getCardsValue() == 17 && dealer.getQuantityOfAces() > 0)) {
                gameOverNoCards(1, "Sorry - round cancelled, not enough cards in dealing shoe to finish this round.");
                dealer.hit(dealingShoe.get(0));
                dealingShoe.remove(0);
            }
            splitWinCases();
        }

        /**
         * Display dealer's all cards and a winner in round where there was no split option chosen
         */
        private void generalWinCases() {
            if (player.getCardsValue() < dealer.getCardsValue() && dealer.getCardsValue() <= 21) {
                //dealer wins
                displayAllCardsDealer();
                showWinner(dealer, "", 0);
            } else if (player.getCardsValue() > dealer.getCardsValue() && player.getCardsValue() <= 21) {
                //player wins
                displayAllCardsDealer();
                showWinner(player, "", 2);
            } else if (player.getCardsValue() == dealer.getCardsValue()) {
                //draw
                displayAllCardsDealer();
                showWinner(null, " have the same value of " + player.getCardsValue(), 0);
            }
        }

        /**
         * Method used in <code>generalWinCase</code>; it prints winner and set new stack in case of player win
         *
         * @param gamer - player (type Player) or dealer (type Dealer)
         * @param text - message to be printed
         * @param multiplicator - bet is multiplied by this number and added to player's stack
         */
        private void showWinner(Playable gamer, String text, double multiplicator) {
            displayAllCardsPlayer();
            if (gamer instanceof Player) {
                printHeaderLowerTop(ANSI_BLUE + text + player.getNick() + " won!!!!" + ANSI_RESET);
                player.setStack(player.getStack() + player.getBet() * multiplicator);
            } else if (gamer instanceof Dealer) {
                printHeaderLowerTop(ANSI_RED + text + player.getNick() + " lost" + ANSI_RESET);
            } else {
                printHeaderLowerTop(ANSI_YELLOW + "Draw, dealer and " + player.getNick() + text + ANSI_RESET);
                player.setStack(player.getStack() + player.getBet());
            }
        }

        /**
         * Display dealer's all cards and a winner in round where split option has been chosen
         */
        private void splitWinCases() {
            displayAllCardsDealer();
            displayAllCardsPlayerSplit();
            if (player.getCardsValue() < dealer.getCardsValue() && dealer.getCardsValue() <= 21) {
                //split1 - dealer wins
                showWinnerOfSplitOne(dealer, "");
            } else if (player.getCardsValue() > dealer.getCardsValue() && player.getCardsValue() <= 21) {
                //split1 - player wins
                showWinnerOfSplitOne(player, "");
            } else if (player.getCardsValue() == dealer.getCardsValue() && player.getCardsValue() <= 21) {
                //split1 - draw
                showWinnerOfSplitOne(null, " have the same value of " + player.getCardsValue());
            } else if (dealer.getCardsValue() > 21 && player.getCardsValue() <= 21) {
                //split1 - player wins if dealer exceeded 21
                showWinnerOfSplitOne(player, " Dealer exceeded 21.");
            }
            if (player.getCardsValueSplit() < dealer.getCardsValue() && dealer.getCardsValue() <= 21) {
                //split2 - dealer wins
                showWinnerOfSplitTwo(dealer, "");
            } else if (player.getCardsValueSplit() > dealer.getCardsValue() && player.getCardsValueSplit() <= 21) {
                //split2 - player wins
                showWinnerOfSplitTwo(player, "");
            } else if (player.getCardsValueSplit() == dealer.getCardsValue() && player.getCardsValueSplit() <= 21) {
                //split2 - draw
                showWinnerOfSplitTwo(null, " have the same value of " + player.getCardsValueSplit());
            } else if (dealer.getCardsValue() > 21 && player.getCardsValueSplit() <= 21) {
                //split2 - player wins if dealer exceeded 21
                showWinnerOfSplitTwo(player, " Dealer exceeded 21.");
            }
        }

        /**
         * Method used in <code>splitWinCase</code>; it prints winner and set new stack in case of player win
         * for split2
         *
         * @param gamer - player (type Player) or dealer (type Dealer)
         * @param text - message to be printed
         */
        private void showWinnerOfSplitOne(Playable gamer, String text) {
            if (gamer instanceof Player) {
                printHeaderLowerTop(ANSI_BLUE + "split 1: " + player.getNick() + " won!!!!" + text + ANSI_RESET);
                player.setStack(player.getStack() + player.getBet() * 2);
            } else if (gamer instanceof Dealer) {
                printHeaderLowerTop(ANSI_RED + "split 1: " + player.getNick() + " lost" + ANSI_RESET);
            } else {
                printHeaderLowerTop(ANSI_YELLOW + "split 1: draw, dealer and " + player.getNick() + text + ANSI_RESET);
                player.setStack(player.getStack() + player.getBet());
            }
        }

        /**
         * Method used in <code>splitWinCase</code>; it prints winner and set new stack in case of player win
         * for split2
         *
         * @param gamer - player (type Player) or dealer (type Dealer)
         * @param text - message to be printed
         */
        private void showWinnerOfSplitTwo(Playable gamer, String text) {
            if (gamer instanceof Player) {
                printHeaderLowerTop(ANSI_BLUE + "split 2: " + player.getNick() + " won!!!!" + text + ANSI_RESET);
                player.setStack(player.getStack() + player.getBetSplit() * 2);
            } else if (gamer instanceof Dealer) {
                printHeaderLowerTop(ANSI_RED + "split 2: " + player.getNick() + " lost" + ANSI_RESET);
            } else {
                printHeaderLowerTop(ANSI_YELLOW + "split 2: draw, dealer and " + player.getNick() + text + ANSI_RESET);
                player.setStack(player.getStack() + player.getBetSplit());
            }
        }
    }
}


