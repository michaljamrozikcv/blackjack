package com.example.blackJack.deck;

public class PlayingCard {
    private Rank rank;
    private Suit suit;

    PlayingCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank.getRank() + suit.getSymbol();
    }

    public int getValue() {
        return rank.getValue();
    }

    public Rank getRank() {
        return rank;
    }
}
