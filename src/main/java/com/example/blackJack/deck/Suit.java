package com.example.blackJack.deck;

public enum Suit {
    CLUB("\u2663"),
    DIAMOND("\u2666"),
    HEART("\u2665"),
    SPADE("\u2660");

    private final String symbol;

    Suit(String symbol) {
        this.symbol = symbol;
    }

    String getSymbol() {
        return symbol;
    }
}
