package com.example.blackJack.deck;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Deck {
    private List<PlayingCard> listOfCards = Stream.of(Rank.values())
            .flatMap(rank -> Stream.of(Suit.values())
                    .map(suit -> new PlayingCard(rank, suit)))
            .collect(Collectors.toList());

    public List<PlayingCard> getListOfCards() {
        return listOfCards;
    }

    @Override
    public String toString() {
        return listOfCards.toString();
    }
}
