package com.example.blackJack;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Gameplay gameplay = Gameplay.createGameplay(scanner);
        while (gameplay.getPlayer().getStack() > 0) {
            gameplay.particularRound(scanner);
        }
    }
}
